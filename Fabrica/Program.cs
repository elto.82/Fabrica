﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrica
{
    class Program
    {
        static void Main(string[] args)
        {
            string tipoProducto, tipoAdicion;
            int cantidad;
            decimal costo;
            PedirDatos(out tipoProducto, out cantidad, out tipoAdicion);
            costo = CalcularCosto(tipoProducto, cantidad, tipoAdicion);
            MostrarResultados(costo);
        }

        private static void PedirDatos(out string tipoProducto, out int cantidad, out string tipoAdicion)
        {
            Console.Write("tipo producto: {e}scitorio, {s}illa, {p}uerta, {v}entana");
            tipoProducto = Console.ReadLine();
            Console.Write("cantidad?");
            cantidad = Convert.ToInt32( Console.ReadLine());
            Console.Write("tipo adicion: {l}aca, {c}romado, {e}nvegecido, c{r}aquelado ");
            tipoAdicion = Console.ReadLine();
        }

        private static void MostrarResultados(decimal costo)
        {
            throw new NotImplementedException();
        }

        private static decimal CalcularCosto(string tipoProducto, int cantidad, string tipoAdicion)
        {
            throw new NotImplementedException();
        }

       
    }
}
